
const clamp = function (number, min, max) {
    number = Number(number);
  
    return Math.min(max, Math.max(min, isNaN(number) ? min : number));
};

let scrollable = document.querySelector('.scrollable');
// let scrollable = document.querySelector('.container-rgb');

let current = 0;
let target = 0;
let ease = 0.1;

// Linear inetepolation used for smooth scrolling and image offset uniform adjustment

// function lerp(start, end, t){
//     // console.log(start * (1 - t ) + end * t)
//     return start * (1 - t ) + end * t;
// }

// init function triggered on page load to set the body height to enable scrolling and EffectCanvas initialised
function init(){
    document.body.style.height = `${scrollable.getBoundingClientRect().height}px`;
}

// translate the scrollable div using the lerp function for the smooth scrolling effect.
// function smoothScroll(){
    // target = window.scrollY;
    // current = lerp(current, target, ease);
    // scrollable.style.transform = `translate3d(0, ${-current}px, 0)`;

//     target = window.scrollY;
//     // console.log(target)
//     current = lerp(current, target, ease);
//     scrollable.style.transform = `translate3d(${-current}px, 0, 0)`;
// }   

var slider1 = document.querySelector(".scrollable");
sliderFirst1 = document.querySelector(".first-slider");
sliderSecond1 = document.querySelector(".second-slider");
class App {
    constructor() {
        this.slider = document.querySelector(".scrollable");
        this.sliderFirst = document.querySelector(".first-slider");
        this.sliderSecond = document.querySelector(".second-slider");

        // this.sliderBounds
    
        this.scroll = {
            current: 0,
            target: 0,
            ease: 0.1
        };
    
        // this.addEvents();
        this.onResize();
        this.createRAF();
        // this.onMouseWheel();
    }
  
    onMouseWheel(event) {
        const normalized = normalizeWheel(event);
        const { pixelY } = normalized;
        // console.log(this.scroll.target)
        this.scroll.target += pixelY;
        // console.log("scrolled")
    }
  
    onResize() {
        // console.log("resized")
        // console.log(this.sliderBounds)

        // if(this.slider) {

            // console.log(this.sliderBounds)
            this.sliderBounds = this.slider.getBoundingClientRect();
        // }
    }
  
    createRAF() {
        this.update();
        // requestAnimationFrame(createRAF);
        // console.log(this)
    }
  
    update() {
        if(this) {
            // console.log(this)
            this.scroll.current +=
            (this.scroll.target - this.scroll.current) * this.scroll.ease;
    
            // arbitrary value to allow more scroll
            const multipliedScroll = this.scroll.current * 1.5;
            // console.log(this.sliderBounds);
            if(this.sliderBounds) {

                const multipliedIndex = (multipliedScroll / this.sliderBounds.width) * 2;
                this.multiplier = Math.floor(multipliedIndex);
                this.sliderFirst.style.transform = `translateX(${
                -multipliedScroll + (this.multiplier * this.sliderBounds.width) / 2
                }px)`;
        
                this.sliderSecond.style.transform = `translateX(${
                -multipliedScroll + (this.multiplier * this.sliderBounds.width) / 2
                }px)`;

            }
    
            requestAnimationFrame(this.update);
        } else {
            app.update()
        }

    }
}

let app = new App();
window.addEventListener("wheel", function(e) {
    app.onMouseWheel(e);
    // console.log(app.onMouseWheel(e));
});
var viewport;
window.addEventListener("resize", function(e) {
    app.onResize();
    // viewport = window.innerWidth;
    // console.log(viewport);
});
// app.createRAF();

class EffectCanvas{
    constructor(){
        this.container = document.querySelector('main');
        this.images = [...document.querySelectorAll('img')];
        this.meshItems = []; // Used to store all meshes we will be creating.
        this.setupCamera();
        this.createMeshItems();
        this.render();
        // console.log(this.target);

        //mouseover distort
        this.mouse = new THREE.Vector2();
        this.prevMouse = new THREE.Vector2();
        this.followMouse = new THREE.Vector2();

        this.mouseSpeed = 0;
        this.targetSpeed = 0;
        this.time = 0;
        // console.log(this)

        // this.getSpeed.bind(this);
    }

    //get mouse event scrolls
    getSpeed() {
        // console.log(this.prevMouse)
        this.mouseSpeed = Math.sqrt(
          (this.prevMouse.x - this.mouse.x) ** 2 +(this.prevMouse.y - this.mouse.y) ** 2
        );
    
        this.targetSpeed -= 0.1 * (this.targetSpeed - this.mouseSpeed);
        this.followMouse.x -= 0.1 * (this.followMouse.x - this.mouse.x);
        this.followMouse.y -= 0.1 * (this.followMouse.y - this.mouse.y);
    
        this.targetSpeed = clamp(this.targetSpeed, 0, 0.05);
    
        this.prevMouse.x = this.mouse.x;
        this.prevMouse.y = this.mouse.y;
    }
    //////////////

    // Getter function used to get screen dimensions used for the camera and mesh materials
    get viewport(){
        let width = window.innerWidth;
        let height = window.innerHeight;
        let aspectRatio = width / height;
        return {
          width,
          height,
          aspectRatio
        };
    }

    setupCamera(){
        // console.log(this)
        
        window.addEventListener('resize', this.onWindowResize.bind(this), false);
    
        // Create new scene
        this.scene = new THREE.Scene();
    
        // Initialize perspective camera
    
        let perspective = 1000;
        const fov = (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI; // see fov image for a picture breakdown of this fov setting.
        this.camera = new THREE.PerspectiveCamera(fov, this.viewport.aspectRatio, 1, 2000)
        this.camera.position.set(0, 0, perspective); // set the camera position on the z axis.
        
        // renderer
        // this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        this.renderer.setSize(this.viewport.width, this.viewport.height); // uses the getter viewport function above to set size of canvas / renderer
        this.renderer.setPixelRatio(window.devicePixelRatio); // Import to ensure image textures do not appear blurred.
        this.container.appendChild(this.renderer.domElement); // append the canvas to the main element

        //post pro
        this.composer = new THREE.EffectComposer( this.renderer );
        this.composer.addPass(new THREE.RenderPass(this.scene, this.camera))
        // this.composer.addPass(new THREE.GlitchPass(THREE.DigitalGlitch))
        // this.composer.addPass(new THREE.ShaderPass(THREE.ColorifyShader))
        this.uniforms = {
            "uResolution": {
                value: new THREE.Vector2(1, window.innerHeight / window.innerWidth)
            },
            "tDiffuse": {
                value: null
            },
            "tMap": {
                value: null
            },
            "uMouse": {
                value: new THREE.Vector2()
            },
            "uVelo": {
                value: 0
            },
            "uAmount": {
                value: 0
            },
            "amount": {
                value: 0
            }
        };
        // this.grainShader = new THREE.ShaderPass(THREE.grainShader);
        this.grainShader = new THREE.ShaderPass({
            uniforms: this.uniforms,
            vertexShader:
                `
                varying vec2 vUv;

                void main() {
                    vUv = uv;
                    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                }
                `,

            fragmentShader:
                `
                precision highp float;

                uniform sampler2D tMap;
                uniform vec2 uResolution;
                uniform vec2 uMouse;
                uniform float uVelo;
                uniform float uAmount;
                uniform float uScrollVelo;

                varying vec2 vUv;

                float circle(vec2 uv, vec2 disc_center, float disc_radius, float border_size) {
                uv -= disc_center;
                uv*= uResolution;
                float dist = sqrt(dot(uv, uv));
                return smoothstep(disc_radius+border_size, disc_radius-border_size, dist);
                }

                float random( vec2 p )
                {
                vec2 K1 = vec2(
                    23.14069263277926,
                    2.665144142690225
                );
                return fract( cos( dot(p,K1) ) * 12345.6789 );
                }

                void main() {
                vec2 newUV = vUv;

                float c = circle(newUV, uMouse, 0.0, 0.6);

                float r = texture2D(tMap, newUV.xy += c * (uVelo * .9)).x;
                float g = texture2D(tMap, newUV.xy += c * (uVelo * .925)).y;
                float b = texture2D(tMap, newUV.xy += c * (uVelo * .95)).z;

                vec4 newColor = vec4(r, g, b, 1.);

                newUV.y *= random(vec2(newUV.y, uAmount));
                newColor.rgb += random(newUV)* 0.10;

                gl_FragColor = newColor;
            }`
        });
        // this.composer.addPass(new THREE.GlitchPass(THREE.DigitalGlitch))
        // this.composer.addPass(new THREE.ShaderPass(THREE.ColorifyShader))
        // this.grainShader.renderToScreen = true;
        // this.composer.addPass(this.grainShader);

    }

    onWindowResize(){
        init();
        this.camera.aspect = this.viewport.aspectRatio; // readjust the aspect ratio.
        this.camera.updateProjectionMatrix(); // Used to recalulate projectin dimensions.
        this.renderer.setSize(this.viewport.width, this.viewport.height); 
    }

    createMeshItems(){
        // Loop thorugh all images and create new MeshItem instances. Push these instances to the meshItems array.
        this.images.forEach(image => {
            let meshItem = new MeshItem(image, this.scene);
            this.meshItems.push(meshItem);
        })
    }

    onMouseMove(event) {
        this.mouse.x = event.clientX / window.innerWidth;
        this.mouse.y = 1 - event.clientY / window.innerHeight;
        // console.log("Mouse");
        // console.log(this.mouse);
    }

    // Animate smoothscroll and meshes. Repeatedly called using requestanimationdrame
    render(){
        // smoothScroll();
        // this.update();
        // this.getSpeed();
        // console.log(this.uniforms.uResolution.value)
        for(let i = 0; i < this.meshItems.length; i++){
            this.meshItems[i].render();
        }
        // this.renderer.render(this.scene, this.camera)
        this.time += 0.05
        this.uniforms.amount.value = this.time;
        // if(this.uniforms.amount.value = this.time){
        //     this.uniforms.amount.value = this.time;
        // }

        // this.uniforms.uAmount.value = this.time;
        // THREE.grainShader.uniforms.uAmount.value = this.time;
        // console.log(THREE.grainShader.uniforms.uAmount.value);
        // THREE.ColorifyShader.uniforms.color.value.r = this.time;
        // console.log(THREE.ColorifyShader.uniforms.color.value.r);
        requestAnimationFrame(this.render.bind(this));
        this.composer.render();
        // console.log(composer)

        // this.composer.render();
    } 
}

const vertexShader = `
    uniform sampler2D uTexture;
    uniform vec2 uOffset;
    uniform float uTime;
    uniform float uSpeed;
    
    varying vec2 vUv;

    float M_PI = 3.141529;

    vec3 deformationCurve(vec3 position, vec2 uv, vec2 offset) {
        position.x = position.x + (sin(uv.y * M_PI) * -offset.x);
        // position.z += sin(position.y / 1440.000000 * 3.141536 + 3.141536 / 2.000000) * 200.000000;

        // position.y = position.y + (sin(uv.x * M_PI) * offset.y);
        // position.y = position.y + offset.x;
        // position.z = position.z + (cos(uv.y * M_PI) * offset.y);
        // position.z = 100.000000 * offset.y;
        // position.z = (sin(position.x * 4.0 + offset.y) * 1.5 + cos(position.y * 2.0 + offset.y) * 1.5);
        // position.z = (sin(position.x * 4.0 + uTime) * 100.5 + cos(position.y * 2.0 + uTime) * 1.5);
        // position.z = (cos(position.y * 2.0 + uTime) * 100.5);
        return position;
    }

    void main() {
        vUv = uv;
        vec3 newPosition = deformationCurve(position, uv, uOffset);
        // newPosition.z += sin(newPosition.y / 1440.000000 * 3.141536 + 3.141536 / 2.000000) * -100.000000;
        gl_Position = projectionMatrix * modelViewMatrix * vec4(newPosition, 1.0); 
    }
`;

const fragmentShader = `
    uniform sampler2D uTexture;
    uniform float uAlpha;
    uniform vec2 uOffset;
    varying vec2 vUv;

    vec3 rgbShift(sampler2D textureimage, vec2 uv, vec2 offset) {
        float r = texture2D(textureimage, uv + (offset * 0.3)).r;
        // float r = texture2D(textureimage, uv).r;
        vec2 gb = texture2D(textureimage, uv ).gb;
        // float g = texture2D(textureimage, uv + offset).g;
        // float b = texture2D(textureimage, uv + offset).b;
        return vec3(r, gb);
        // return vec3(r, g, b);
    }

    void main() {
        vec3 color = rgbShift(uTexture, vUv, uOffset);
        gl_FragColor = vec4(color, uAlpha);
    }
`;

// console.log(this)

class MeshItem{
    // Pass in the scene as we will be adding meshes to this scene.
    constructor(element, scene){
        this.element = element;
        this.scene = scene;
        this.offset = new THREE.Vector2(0,0); // Positions of mesh on screen. Will be updated below.
        this.sizes = new THREE.Vector2(0,0); //Size of mesh on screen. Will be updated below.
        this.createMesh();
        // this.rot = 0;
    }

    getDimensions(){
        const {width, height, top, left} = this.element.getBoundingClientRect();
        this.sizes.set(width, height);
        this.offset.set(left - window.innerWidth / 2 + width / 2, -top + window.innerHeight / 2 - height / 2); 
    }

    createMesh(){
        this.geometry = new THREE.PlaneBufferGeometry(1,1,100,100);
        this.imageTexture = new THREE.TextureLoader().load(this.element.src);
        // console.log(this.element.src);
        this.uniforms = {
            uTexture: {
                //texture data
                value: this.imageTexture
            },
            uOffset: {
                //distortion strength
                value: new THREE.Vector2(0.0, 0.0)
            },
            uAlpha: {
                //opacity
                value: 1.0
            },
            uSpeed: { 
                value: 0 
            },
            uTime: { 
                value: 0 
            }
        };
        this.material = new THREE.ShaderMaterial({
            uniforms: this.uniforms,
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            transparent: true,
            // wireframe: true,
            side: THREE.DoubleSide
        })
        this.mesh = new THREE.Mesh( this.geometry, this.material );
        this.getDimensions(); // set offset and sizes for placement on the scene
        this.mesh.position.set(this.offset.x, this.offset.y, 0);
		this.mesh.scale.set(this.sizes.x, this.sizes.y, 1);

        this.scene.add( this.mesh );
    }

    map (num, min1, max1, min2, max2, round = false) {
        const num1 = (num - min1) / (max1 - min1)
        const num2 = (num1 * (max2 - min2)) + min2

        if (round) return Math.round(num2)

        return num2
    }
    
    render(){
        this.rot += 0.5;
        // this function is repeatidly called for each instance in the aboce 
        this.getDimensions();
        this.mesh.position.set(this.offset.x, this.offset.y, 0);
		this.mesh.scale.set(this.sizes.x, this.sizes.y, 1);
		// this.mesh.rotation.z = this.rot;
        // this.mesh.position.z = this.map(this.mesh.position.x, -7200, 7200, Math.PI, -Math.PI);
        this.mesh.position.z = Math.sin(this.mesh.position.x / 1440 * Math.PI + Math.PI / 2.0) * (-this.mesh.position.x * 0.2);
        // this.mesh.position.z = Math.sin(this.mesh.position.x * 4.0 + uTime) * 100.5 + Math.cos(this.mesh.position.y * 2.0 + uTime) * 1.5);
        this.uniforms.uTime.value += 0.04
        // this.mesh.position.z = Math.sin(this.mesh.position.x * 4.0 + this.uniforms.uTime.value) * 100.5 + Math.cos(this.mesh.position.y * 2.0 + this.uniforms.uTime.value) * 1.5;

        // console.log(Math.sin(this.mesh.position.y / 1440 * Math.PI + Math.PI / 2.0) * -1)
        // console.log(viewport)
        // this.mesh.scale.set();
        // console.log(this.mesh.normal)
		// this.mesh.scale.set(this.offset.value)
        // console.log(this.uniforms.uTime.value)
        // this.uniforms.uOffset.value.set(this.offset.x * 0.0, -(app.scroll.target - app.scroll.current) * 0.0003 )
        this.uniforms.uOffset.value.set((app.scroll.target - app.scroll.current) * 0.0002,  -(app.scroll.target - app.scroll.current) * 0.0000 );
    }
}

init()
// new App()
let effect = new EffectCanvas();

window.addEventListener("mousemove", function(e) {
    // console.log("Mouse")
    effect.onMouseMove(e);
});
